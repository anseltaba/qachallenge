Feature: Paylocity Employee Dashboard

  Background: Login steps for each scenario
    Given an Employer
    And I am on the Benefits Dashboard page

  @add
  Scenario: Add Employee
    When I select Add Employee
    Then I should be able to enter employee details
    And the employee should save
    And I should see the employee in the table
    And the benefit cost calculations are correct
    And the user logout and close browser

  @edit
  Scenario: Edit Employee
      When I select the Action Edit
     Then I can edit employee details
     And the data should change in the table
    And the user logout and close browser

  @delete
  Scenario: Delete Employee
    When I click the Action X
   Then the employee should be deleted
   And the user logout and close browser
