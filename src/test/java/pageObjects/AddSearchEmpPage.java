package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.PageFactory;
import utilities.WaitHelper;

import java.util.List;

public class AddSearchEmpPage {
  WaitHelper waitHelper;

  public AddSearchEmpPage(WebDriver driver){

    PageFactory.initElements(driver, this);

    waitHelper = new WaitHelper(driver);
  }

  @FindBy(id="firstName")
  WebElement firstName;

  @FindBy(id="lastName")
  WebElement lastName;

  @FindBy(id="dependants")
  WebElement dependants;

  @FindBy(id="addEmployee")
  WebElement addBtn;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[2]")
  List<WebElement> names;

  public void setFirstName(String firstN){
    waitHelper.WaitForElement(firstName, 10);
    firstName.clear();
    firstName.sendKeys(firstN);
  }

  public void setLastName(String lastN){
    waitHelper.WaitForElement(lastName, 10);
    lastName.clear();
    lastName.sendKeys(lastN);
  }

  public void setDependants(String dep){
    waitHelper.WaitForElement(dependants, 10);
    dependants.clear();
    dependants.sendKeys(dep);
  }

  public void saveEmployee(){
    addBtn.click();
  }

  @FindBy(xpath = "//div[@id='employeeModal']//span[contains(text(),'×')]")
  WebElement closeBtn;

  public void closePopupWind(){
    closeBtn.click();
  }

  @FindBy(xpath = "//table[@id='employeesTable']")
  WebElement empTable;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr")
  List<WebElement> tableRows;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td")
  List<WebElement> tableColumns;

  public int getNumOfRows(){
    return tableRows.size();
  }

  public int getNumOfColumns(){
    return tableColumns.size();
  }



//----------------------------------------------------Search by Employee id   --------------------
  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[1]")
  public List<WebElement> empIds;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr[1]/td[1]")
  public WebElement createdEmployeeID;

 public boolean isEmployeeCreated(){
   WaitHelper.WaitForElement(createdEmployeeID, 3);
   for(WebElement id: empIds){
     if(id.getText().equals(createdEmployeeID.getText())){
       return true;
     }
   }
   return false;

 }

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[6]")
  List<WebElement> grossPayTotal;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[7]")
  List<WebElement> benefitCostTotal;

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[8]")
  List<WebElement> netPayTotal;

  public boolean verifyCalculateBenefitCost() throws InterruptedException {
    int size = grossPayTotal.size();
    Thread.sleep(3000);
    for(int i = 0; i < size; i++){
      double benefitCostDouble = Double.parseDouble(benefitCostTotal.get(i).getText().trim());
      double grossPayDouble = Double.parseDouble(grossPayTotal.get(i).getText().trim());
      double netPayDouble = Double.parseDouble(netPayTotal.get(i).getText().trim());
//      System.out.println("Benefit cost: "+ benefitCostDouble);//57.69
//      System.out.println("Gross cost: "+ grossPayDouble); //2000.0
//      System.out.println("Net pay: "+netPayDouble); //1942.31
      if(grossPayDouble != Double.sum(benefitCostDouble, netPayDouble)){
        return false;

      }
    }
    return true;
    }

  //    ----------------- Action/Update Button  -----------------------

 @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr[1]/td[9]/i[@class='fas fa-edit']")
 WebElement actionBtns;

  @FindBy(id="updateEmployee")
  WebElement updateBtn;

  public void clickActionBtn() throws InterruptedException {
   Thread.sleep(3000);
    actionBtns.click();

  }

  public void clickUpdateBtn(){
    updateBtn.click();
  }


  //    ----------------- Action/Delete Button  -----------------------

 @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr/td[9]/i[@class='fas fa-times']")
 List<WebElement> deleteBtns;



  @FindBy(id = "deleteEmployee")
  WebElement deleteEmpBtn;

  public void clickDeleteMultipleBtn(){

    for(int j = 0; j < deleteBtns.size(); j++){
      deleteBtns.get(j).click();
      clickDeleteEmpBtn();

    }
  }

  @FindBy(xpath = "//table[@id='employeesTable']/tbody/tr[1]/td[9]/i[@class='fas fa-times']")
  WebElement deleteButon;

  public void clickDeleteBtn() throws InterruptedException {
    Thread.sleep(3000);
    deleteButon.click();
    clickDeleteEmpBtn();
  }

  public boolean isEmployeeDeleted(){
    WaitHelper.WaitForElement(createdEmployeeID, 3);
    for(WebElement id: empIds){
      if(!id.getText().equals(createdEmployeeID.getText())){
        return true;
      }
    }
    return false;

  }

  public void clickDeleteEmpBtn(){
    waitHelper.WaitForElement(deleteEmpBtn, 10);
    deleteEmpBtn.click();
  }
}
