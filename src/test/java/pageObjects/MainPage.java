package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.WaitHelper;

import java.util.List;


public class MainPage {

  public WebDriver ldriver;
  WaitHelper waitHelper;

  public MainPage(WebDriver rdriver){
    ldriver = rdriver;
    PageFactory.initElements(rdriver, this);
    waitHelper = new WaitHelper(ldriver);
  }

  @FindBy(xpath = "//*[@id=\"Username\"]")
  WebElement usernameInput;

   @FindBy(name="Password")
  WebElement passwordInput;

   @FindBy(xpath = "//button[.='Log In']")
  WebElement loginBtn;

   public void loginHPage(String userN, String pass){
     usernameInput.sendKeys(userN);
     passwordInput.click();
     passwordInput.sendKeys(pass);
     loginBtn.click();
   }

   @FindBy(id= "add")
  WebElement addEmpBtn;

   public void clickAddEmpBtn(){
     addEmpBtn.click();
   }

   @FindBy(xpath = "//a[contains(text(),'Log Out')]")
  WebElement logoutBtn;

   public void clickLogoutBtn(){
     //waitHelper.WaitForElement(loginBtn, 30);
     logoutBtn.click();
   }
}
