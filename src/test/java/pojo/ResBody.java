package pojo;

public class ResBody {
  private String partitionKey;
  private String sortKey;
  private String username;
  private String id;
  private String firstName;
  private String lastName;
  private String dependants;

  public String getPartitionKey() {
    return partitionKey;
  }

  public void setPartitionKey(String partitionKey) {
    this.partitionKey = partitionKey;
  }

  public String getSortKey() {
    return sortKey;
  }

  public void setSortKey(String sortKey) {
    this.sortKey = sortKey;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getDependants() {
    return dependants;
  }

  public void setDependants(String dependants) {
    this.dependants = dependants;
  }

  public String getExpiration() {
    return expiration;
  }

  public void setExpiration(String expiration) {
    this.expiration = expiration;
  }

  public String getSalary() {
    return salary;
  }

  public void setSalary(String salary) {
    this.salary = salary;
  }

  public String getGross() {
    return gross;
  }

  public void setGross(String gross) {
    this.gross = gross;
  }

  public String getBenefitsCost() {
    return benefitsCost;
  }

  public void setBenefitsCost(String benefitsCost) {
    this.benefitsCost = benefitsCost;
  }

  public String getNet() {
    return net;
  }

  public void setNet(String net) {
    this.net = net;
  }

  private String expiration;
  private String salary;
  private String gross;
  private String benefitsCost;
  private String net;

}
