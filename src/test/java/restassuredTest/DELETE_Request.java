package restassuredTest;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.*;
import pojo.ResBody;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class DELETE_Request {
  public Response response;
  public static String empId;
  public static List<String> listOfEmpIds = new ArrayList<>();

  @Test
  public void deleteEmployee(){

    RestAssured.baseURI = "https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/";
    RestAssured.basePath = "Prod/api/employees";

    response = given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
            .when().get().then().extract().response();
    JsonPath js = response.jsonPath();
    List<ResBody> responseBody = js.getList("", ResBody.class);
    for (int i = 0; i < responseBody.size(); i++) {
      listOfEmpIds.add(responseBody.get(i).getId());
    }

    for (int i = 0; i < listOfEmpIds.size(); i++) {
      empId = listOfEmpIds.get(i);
    }
    System.out.println("Employee id: " + empId);

    given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
            .when()
               .delete(empId)
            .then()
              .statusCode(200)
              .log().all();
  }
}
