package restassuredTest;

import io.restassured.RestAssured;
import org.testng.annotations.*;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static restassuredTest.POST_Request.newPostId;

public class GET_POST_Json {


  @Test(priority = 3)
  public void testGetReq(){
    baseURI = "https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/";
    basePath="Prod/api/employees";

    given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
            .when()
              .get("d18e6302-078b-49ca-97d8-9986245bf573")
            .then()
              .statusCode(200)
            .log().all();
  }
}
