package restassuredTest;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import pojo.ResBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class GET_Request {
  public Response response;
  public static String empId;
  public static List<String> listOfEmpIds = new ArrayList<>();

  @Test(priority = 1)
  public void getEmployeeList() {
    baseURI = "https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/Prod/api/employees";
    response = given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
            .when().get().then().extract().response();
    JsonPath js = response.jsonPath();
    List<ResBody> responseBody = js.getList("", ResBody.class);
    for (int i = 0; i < responseBody.size(); i++) {
      listOfEmpIds.add(responseBody.get(i).getId());
    }

    for (int i = 0; i < listOfEmpIds.size(); i++) {
      empId = listOfEmpIds.get(i);
    }
    System.out.println(listOfEmpIds.size() + " list of Emp ids");
    System.out.println("Employee id: " + empId);
    given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
            .when().get(baseURI)
            .then()
            .statusCode(200)
            .statusLine("HTTP/1.1 200 OK")
            .and()
            .body("id", hasItems(empId));

  }
}
