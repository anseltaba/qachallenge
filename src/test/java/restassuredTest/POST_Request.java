package restassuredTest;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.*;
import pojo.ResBody;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class POST_Request {
  public static String newPostId;
  public static HashMap empData = new HashMap();


  @BeforeClass
  public void employeeData(){
    empData.put("firstName", "Nat");
    empData.put("lastName", "Roma");
    empData.put("dependants", 3);

    RestAssured.baseURI = "https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/Prod/api/employees";

  }

  @Test(priority = 2)
  public void testPost(){

    given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
             .contentType("application/json")
             .body(empData)
           .when()
             .post()
           .then()
             .statusCode(200)   //201
           .and()
             .body("firstName", equalTo("Nat"))
           .and()
             .body("lastName", equalTo("Roma"))
            .log().all();


  }



}
