package restassuredTest;

import io.restassured.RestAssured;
import org.testng.annotations.*;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static restassuredTest.POST_Request.newPostId;


import java.util.HashMap;

public class PUT_Request {
  public static HashMap updateData = new HashMap();
  String emp_id = "2845963b-ee95-4545-91ee-35ee47e8aa2c";

  @BeforeClass
  public void putData(){
    updateData.put("id", emp_id);
    updateData.put("firstName", "W");
    updateData.put("lastName", "M");
    updateData.put("dependants", 4);

    RestAssured.baseURI = "https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/Prod/api/employees";

  }


  @Test
  public void testPut(){
    given().header("Authorization", String.format("Basic %s", "VGVzdFVzZXIyMzprWHd9VVthSF5bNDk="))
              .contentType("application/json")
              .body(updateData)
            .when()
              .put()
            .then()
              .statusCode(200)
//            .and()
//              .body("id", equalTo(emp_id))
            .and()
              .body("firstName", equalTo("W"))
            .and()
              .body("lastName", equalTo("M"))
            .log().all(); //to check log

  }
}
