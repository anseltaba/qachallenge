package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.AddSearchEmpPage;
import pageObjects.MainPage;
import utilities.Driver;

import java.util.Properties;

public class BaseClass {

  public WebDriver driver;
  public MainPage mp;
  public AddSearchEmpPage addSearchEmpPage;
  public Properties configProp;
}
