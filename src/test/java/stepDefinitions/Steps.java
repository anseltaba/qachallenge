package stepDefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import pageObjects.AddSearchEmpPage;
import pageObjects.MainPage;
import utilities.Driver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Steps extends BaseClass{

  @Before
  public void setup(){
    driver = Driver.getDriver();
    addSearchEmpPage = new AddSearchEmpPage(driver);
  }


  @Given("an Employer")
  public void an_Employer() throws IOException {
    configProp = new Properties();
    FileInputStream configPropfile = new FileInputStream("config.properties");
    configProp.load(configPropfile);
    driver = Driver.getDriver();
    mp = new MainPage(driver);
    driver.get("https://wmxrwq14uc.execute-api.us-east-1.amazonaws.com/Prod/Account/LogIn");//configProp.getProperty("url")
    mp.loginHPage(configProp.getProperty("username"), configProp.getProperty("password")); //need properties file

  }

  @And("I am on the Benefits Dashboard page")
  public void i_am_on_the_Benefits_Dashboard_page() {
    String exceptedTitle = "Employees - Paylocity Benefits Dashboard";
    Assert.assertEquals(driver.getTitle(), exceptedTitle);
  }

  @When("I select Add Employee")
  public void i_select_Add_Employee() {
     mp.clickAddEmpBtn();
  }


  //Steps for adding and searching employee
  @Then("I should be able to enter employee details")
  public void i_should_be_able_to_enter_employee_details() {
    addSearchEmpPage.setFirstName("John");
    addSearchEmpPage.setLastName("Doe");
    addSearchEmpPage.setDependants("1");

  }

  @Then("the employee should save")
  public void the_employee_should_save() {
    addSearchEmpPage.saveEmployee();
    addSearchEmpPage.closePopupWind();
  }

  @Then("I should see the employee in the table")
  public void i_should_see_the_employee_in_the_table() {
    Assert.assertTrue(addSearchEmpPage.isEmployeeCreated(), "Employee not created!");
  }

  @Then("the benefit cost calculations are correct")
  public void the_benefit_cost_calculations_are_correct() throws InterruptedException {
  boolean resultOfBenefitCalc = addSearchEmpPage.verifyCalculateBenefitCost();
  Assert.assertTrue(resultOfBenefitCalc);
  }


  @When("I select the Action Edit")
  public void i_select_the_Action_Edit() throws InterruptedException {
    addSearchEmpPage = new AddSearchEmpPage(driver);
    addSearchEmpPage.clickActionBtn();
    addSearchEmpPage.setFirstName("Jimmy");
    addSearchEmpPage.setLastName("Tom");
    addSearchEmpPage.setDependants("2");
    addSearchEmpPage.clickUpdateBtn();
  }

  @Then("I can edit employee details")
  public void i_can_edit_employee_details() {
//This method I need to use together with i_select_the_Action_Edit()
  }

  @Then("the data should change in the table")
  public void the_data_should_change_in_the_table() {
    Assert.assertTrue(addSearchEmpPage.isEmployeeCreated(), "Employee not created!");
  }

  @When("I click the Action X")
  public void i_click_the_Action_X() throws InterruptedException {
     addSearchEmpPage = new AddSearchEmpPage(driver);
     addSearchEmpPage.clickDeleteBtn();
  }

  @Then("the employee should be deleted")
  public void the_employee_should_be_deleted() {
    Assert.assertTrue(addSearchEmpPage.isEmployeeDeleted(), "Employee not deleted!");
  }



  @Then("the user logout and close browser")
  public void the_user_logout_and_close_browser() {
      mp.clickLogoutBtn();
  }
}
