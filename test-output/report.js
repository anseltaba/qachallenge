$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:Features/Main.feature");
formatter.feature({
  "name": "Paylocity Employee Dashboard",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "Login steps for each scenario",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "an Employer",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.an_Employer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the Benefits Dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.i_am_on_the_Benefits_Dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add Employee",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@add"
    }
  ]
});
formatter.step({
  "name": "I select Add Employee",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_select_Add_Employee()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to enter employee details",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_should_be_able_to_enter_employee_details()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee should save",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_employee_should_save()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see the employee in the table",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.i_should_see_the_employee_in_the_table()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the benefit cost calculations are correct",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_benefit_cost_calculations_are_correct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user logout and close browser",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_user_logout_and_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Login steps for each scenario",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "an Employer",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.an_Employer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the Benefits Dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.i_am_on_the_Benefits_Dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit Employee",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@edit"
    }
  ]
});
formatter.step({
  "name": "I select the Action Edit",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_select_the_Action_Edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can edit employee details",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_can_edit_employee_details()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the data should change in the table",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_data_should_change_in_the_table()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user logout and close browser",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_user_logout_and_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Login steps for each scenario",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "an Employer",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.an_Employer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the Benefits Dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.i_am_on_the_Benefits_Dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Delete Employee",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@delete"
    }
  ]
});
formatter.step({
  "name": "I click the Action X",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_click_the_Action_X()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee should be deleted",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.the_employee_should_be_deleted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user logout and close browser",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.the_user_logout_and_close_browser()"
});
formatter.result({
  "status": "passed"
});
});